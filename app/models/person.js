const mongoose = require('mongoose');

const PersonSchema = mongoose.Schema({
    firstName: String,
    lastName: String,
    middleName: String,
    birthday: Date,
    twitterId: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Person', PersonSchema);