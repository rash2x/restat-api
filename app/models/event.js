const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
    kind: { type: String, enum: ['arrest', 'protest'] }
}, {
    timestamps: true
});

module.exports = mongoose.model('Event', EventSchema);