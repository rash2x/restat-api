const mongoose = require('mongoose');
const Event = require('./event');

const ArrestEvent = Event.discriminator('Arrest', mongoose.Schema({
    startDate: Date,
    endDate: Date,
    location: {
        lat: Number,
        lng: Number
    },
    personId: { type: mongoose.Schema.Types.ObjectId, ref: 'Person' }
}, { discriminatorKey: 'kind' }));

module.exports = ArrestEvent;