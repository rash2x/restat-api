const express = require('express');
const dbConfig = require('./configs/db.js');
const mongoose = require('mongoose');
const cors = require('cors');

const port = process.env.PORT || 3000;
const app = express();
const router = express.Router();

const whitelist = ['http://localhost:3001', 'http://46.101.99.95:3000/'];
const corsOptions = {
    origin: function (origin, callback) {
        console.log(origin)
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    },
    credentials: true
}

module.exports = app;

require('./configs/express')(app);
require('./configs/routes')(app);


app
    .use(cors(corsOptions))
    .use('/api', router)
    .listen(port, () => {
        console.log('Server is listening on port 3000');
    });

mongoose.Promise = global.Promise;
mongoose.set('debug', true);
mongoose.connect(dbConfig.url)
    .then(() => {
        console.log('Successfully connected to the database');
    }).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});