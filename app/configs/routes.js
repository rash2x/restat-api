module.exports = (app) => {
    const persons = require('../controllers/person.js');
    const events = require('../controllers/event.js');
    const arrests = require('../controllers/arrest.js');

    // persons
    app.post('/api/persons', persons.create);
    app.get('/api/persons', persons.findAll);
    app.get('/api/persons/:personId', persons.findOne);
    app.put('/api/persons/:personId', persons.update);
    app.delete('/api/persons/:personId', persons.delete);

    // events
    app.get('/api/events', events.findAll);
    app.get('/api/events/:eventId', events.findOne);
    app.delete('/api/events/:eventId', events.delete);

    // arrests
    app.post('/api/events/arrests/', arrests.create);
    app.put('/api/events/arrests/:eventId', arrests.update);
};