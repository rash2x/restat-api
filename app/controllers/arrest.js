const Arrest = require('../models/arrest.js');

// TODO: validate

exports.create = (req, res) => {
    const arrest = new Arrest({
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        location: {
            lat: req.body.location.lat,
            lng: req.body.location.lng
        },
        personId: req.body.personId,
    });

    arrest.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || 'Some error occurred while creating the Note.'
        });
    });
};

exports.findAll = (req, res) => {
    Arrest.find()
        .then(arrests => {
            res.send(arrests);
        }).catch(err => {
        res.status(500).send({
            message: err.message || 'Some error occurred while retrieving notes.'
        });
    });
};

exports.update = (req, res) => {
    // Find note and update it with the request body
    Arrest.findByIdAndUpdate(req.params.eventId, {
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        location: {
            lat: req.body.location.lat,
            lng: req.body.location.lng
        },
        personId: req.body.personId,
    },{ new: true })
        .then(arrest => {
            if (!arrest) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.eventId
                });
            }
            res.send(arrest);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.eventId
            });
        }
        return res.status(500).send({
            message: 'Error updating note with id ' + req.params.eventId
        });
    });
};