const Person = require('../models/person.js');

// TODO: validate

exports.create = (req, res) => {
    const person = new Person({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        middleName: req.body.middleName,
        birthday: req.body.birthday,
        twitterId: req.body.twitterId,
    });

    person.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || 'Some error occurred while creating the Note.'
        });
    });
};

exports.findAll = (req, res) => {
    Person.find()
        .then(persons => {
            res.send(persons);
        }).catch(err => {
        res.status(500).send({
            message: err.message || 'Some error occurred while retrieving notes.'
        });
    });
};

exports.findOne = (req, res) => {
    Person.findById(req.params.personId)
        .then(person => {
            if (!person) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.personId
                });
            }
            res.send(person);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.personId
            });
        }
        return res.status(500).send({
            message: 'Error retrieving note with id ' + req.params.personId
        });
    });
};

exports.update = (req, res) => {
    // Find note and update it with the request body
    Person.findByIdAndUpdate(req.params.personId, {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        middleName: req.body.middleName,
        birthday: req.body.birthday,
        twitterId: req.body.twitterId,
    },{ new: true })
        .then(person => {
            if (!person) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.personId
                });
            }
            res.send(person);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.personId
            });
        }
        return res.status(500).send({
            message: 'Error updating note with id ' + req.params.personId
        });
    });
};


exports.delete = (req, res) => {
    Person.findByIdAndRemove(req.params.personId)
        .then(person => {
            if (!person) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.personId
                });
            }
            res.send({ message: 'Note deleted successfully!' });
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.personId
            });
        }
        return res.status(500).send({
            message: 'Could not delete note with id ' + req.params.personId
        });
    });
};