const Event = require('../models/event.js');

// TODO: validate

exports.findAll = (req, res) => {
    Event.find()
        .then(persons => {
            res.send(persons);
        }).catch(err => {
        res.status(500).send({
            message: err.message || 'Some error occurred while retrieving notes.'
        });
    });
};

exports.findOne = (req, res) => {
    Event.findById(req.params.personId)
        .then(person => {
            if (!person) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.personId
                });
            }
            res.send(person);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.personId
            });
        }
        return res.status(500).send({
            message: 'Error retrieving note with id ' + req.params.personId
        });
    });
};


exports.delete = (req, res) => {
    Event.findByIdAndRemove(req.params.personId)
        .then(person => {
            if (!person) {
                return res.status(404).send({
                    message: 'Note not found with id ' + req.params.personId
                });
            }
            res.send({ message: 'Note deleted successfully!' });
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: 'Note not found with id ' + req.params.personId
            });
        }
        return res.status(500).send({
            message: 'Could not delete note with id ' + req.params.personId
        });
    });
};